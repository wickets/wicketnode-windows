import express ,{Application, urlencoded}from "express";
import bodyparser from "body-parser";
import morgan  from "morgan";
import path from "path"

//imports 
 import rutasClients from "./routes/auth";



export class App {

    private app:Application;
   
    //Quickets2019.

    constructor(private port?:number | string) {
    
        this.app=express();
        this.settings();
        this.midlewares();
        this.routes();
       
    }
    //configuraciones
    private settings(){
        this.app.set('port',process.env.PORT || this.port || 3000 );
    }
 
    //midlewares
    private midlewares(){
        this.app.use(express.static(path.join(__dirname,'public')))
        this.app.use(morgan('dev'));//mensajes de desarrollo por consola
        this.app.use(express.urlencoded({extended:false}))
        this.app.use(bodyparser.json());
    }

    private routes(){ 
        this.app.use('/auth',rutasClients);
        
    }

    async listen(){
         await  this.app.listen(this.app.get('port'))
         //cuando ya esta listo el servidor mostramos :
         console.log(`Server on port  ${this.port}`);
         
    }
}