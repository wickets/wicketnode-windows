const form=document.querySelector("form");

window.addEventListener("DOMContentLoaded",()=>{

form.addEventListener("submit",(ev)=>{
   
    ev.preventDefault();

   /****************************************************
            login  con google---firebase
   ********************************************************/
    const provider = new firebase.auth.GoogleAuthProvider();

    auth.signInWithPopup(provider)
     .then((result) => { 
      console.log(result);//data google firebase

       var credential = result.credential;
       var token = credential.accessToken;
       var user = result.user;

       const obj={
                token:token,
                user:user,
                credential:credential
            }

      const dataUser=JSON.stringify(obj);
    
    //calling our API (end point)
     fetch('http://localhost:3080/auth/login-wgoo',{
     method:'POST',
     body:dataUser,
     headers:{
        'Content-Type': 'application/json'
      }
         })
        .then(response => response.json())
        .then(data => console.log(data));
    }).catch((error) => {
        console.log(error);
 });

    
     
});
  
})